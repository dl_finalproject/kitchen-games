import random

from kitchen import kitchen
from kitchen.actions import PLAYER_ACTIONS, TWO_PLAYER_ACTIONS
from kitchen.rewards import scoring


class Environment():
    def __init__(self, path_to_layout, num_of_steps):
        self.path_to_layout = path_to_layout
        self.kit = kitchen.Kitchen(path_to_layout, scoring)
        self.kit.set_agents_location_randomly()
        self.num_of_steps = num_of_steps
        self.current_num_of_steps = num_of_steps
        self.observation_space = self.kit.state_space
        self.action_space = len(TWO_PLAYER_ACTIONS)

    def step(self, action_tuple):
        reward = 0
        agent_num = 1
        done = False

        # Progress all the used ovens
        self.kit.do_all_cook_steps()

        # Do all agents actions
        for action in action_tuple:

            if action == "UP":
                reward = reward + self.kit.move(agent_num, "UP")
            elif action == "DOWN":
                reward = reward + self.kit.move(agent_num, "DOWN")
            elif action == "LEFT":
                reward = reward + self.kit.move(agent_num, "LEFT")
            elif action == "RIGHT":
                reward = reward + self.kit.move(agent_num, "RIGHT")
            elif action == "USE":
                reward = reward + self.kit.use(agent_num, self.kit.get_use_location(agent_num))
            agent_num = agent_num + 1

        reward = reward - 1

        # Progress all the used ovens
        self.kit.do_all_cook_steps()

        self.current_num_of_steps = self.current_num_of_steps - 1

        if self.current_num_of_steps == 0:
            done = True

        return self.kit.layout_as_vector, reward, done

    @staticmethod
    def chose_random_action():
        return random.randint(0, len(TWO_PLAYER_ACTIONS) - 1)

    def reset(self):
        self.kit = kitchen.Kitchen(self.path_to_layout, scoring)
        self.kit.set_agents_location_randomly()
        self.current_num_of_steps = self.num_of_steps

        return self.kit.layout_as_vector

    def reset_with_agents_locations(self, agent1_x, agent1_y, agent2_x, agent2_y):
        self.kit = kitchen.Kitchen(self.path_to_layout, scoring)
        self.kit.set_agents_first_locations(kitchen.Location(agent1_x, agent1_y), kitchen.Location(agent2_x, agent2_y))
        self.current_num_of_steps = self.num_of_steps

        return self.kit.layout_as_vector

    def render(self):
        print(self.kit.layout)
