import random

from kitchen import kitchen
from kitchen.actions import PLAYER_ACTIONS, TWO_PLAYER_ACTIONS
from kitchen.rewards import scoring

class Environment1():
    def __init__(self, path_to_layout, num_of_steps):
        self.path_to_layout = path_to_layout
        self.kit = kitchen.Kitchen(path_to_layout, scoring)
        self.kit.set_agent_location_randomly()
        #self.kit.set_agent_location(kitchen.Location(2, 2), 1)
        self.num_of_steps = num_of_steps
        self.current_num_of_steps = num_of_steps
        self.observation_space = self.kit.state_space
        self.action_space = len(PLAYER_ACTIONS)

    def step(self, action):
        reward = 0
        served = 0
        agent_num = 1
        done = False

        # Progress all the used ovens
        self.kit.do_all_cook_steps()

        if action == "UP":
            reward = reward + self.kit.move(agent_num, "UP")
        elif action == "DOWN":
            reward = reward + self.kit.move(agent_num, "DOWN")
        elif action == "LEFT":
            reward = reward + self.kit.move(agent_num, "LEFT")
        elif action == "RIGHT":
            reward = reward + self.kit.move(agent_num, "RIGHT")
        elif action == "USE":
            reward = reward + self.kit.use(agent_num, self.kit.get_use_location(1))
        agent_num = agent_num + 1

        reward = reward - 1

        # Progress all the used ovens
        self.kit.do_all_cook_steps()

        self.current_num_of_steps = self.current_num_of_steps - 1

        served = self.kit.served

        if self.current_num_of_steps == 0:
            done = True

        return self.kit.layout_as_vector, reward, done

    @staticmethod
    def chose_random_action():
        return random.randint(0, len(PLAYER_ACTIONS) - 1)

    def reset(self):
        self.kit = kitchen.Kitchen(self.path_to_layout, scoring)
        self.kit.set_agent_location_randomly()
        self.current_num_of_steps = self.num_of_steps

        return self.kit.layout_as_vector

    def reset_with_agent_location(self, agent_x, agent_y):
        self.kit = kitchen.Kitchen(self.path_to_layout, scoring)
        self.kit.set_agent_location(kitchen.Location(agent_x, agent_y), 1)
        self.current_num_of_steps = self.num_of_steps

        return self.kit.layout_as_vector

    def render(self):
        print(self.kit.layout)
