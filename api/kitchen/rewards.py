scoring = {
    "move": 0,
    "moving": 0,
    "pick": 100,
    "pick_from_table": 0,
    "put_bad": -20,
    "put_good": 100,
    "cut": 200,
    "cook_start": 400,
    "illegal": -20,
    "cook_finished": 800,
    "pick_cut": 200,
    "cook_bad": -10,
    "serve_good": 10000,
    "finish_meal": 3000,
    "throw": -300
}
