scoring = {
    "moving": 0,
    "pick": 1,
    "pick_from_table": 1,
    "put_bad": 0,
    "cut": 50,
    "cook_start": 100,
    "illegal": 0,
    "cook_finished": 200,
    "pick_cut": 100,
    "serve_good": 10000,
    "finish_meal": 3000,
}
