PLAYER_ACTIONS = [
    'NOOP',
    'UP',
    'DOWN',
    'LEFT',
    'RIGHT',
    'USE'
]

TWO_PLAYER_ACTION = [[(PLAYER_ACTIONS[j], PLAYER_ACTIONS[i]) for i in range(0, len(PLAYER_ACTIONS))] for j in
                      range(0, len(PLAYER_ACTIONS))]
TWO_PLAYER_ACTIONS = [item for sublist in TWO_PLAYER_ACTION for item in sublist]

