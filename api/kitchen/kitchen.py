from random import sample

import numpy as np

from kitchen.dictionary import layout_dictionary, char_dictionary

class Kitchen:
    def __init__(self, path_to_layout, scoring):
        self.scoring = scoring
        self.start_layout = np.loadtxt(path_to_layout, dtype='str', delimiter=' ')
        self.layout = np.loadtxt(path_to_layout, dtype='str', delimiter=' ')
        self.layout = self.layout.astype('U256')
        self.rows = self.layout.shape[0]
        self.columns = self.layout.shape[1]
        self.layout_as_vector = np.zeros((self.rows, self.columns, len(char_dictionary)))
        self.state_space = self.rows * self.columns * len(char_dictionary)

        self.agents = {
            1: Agent(None, None, "DOWN"),
            2: Agent(None, None, "DOWN")
        }

        self.floors = []
        self.ovens = []
        self.cuttings = []
        self.valid_meals = [layout_dictionary["second_product"] + layout_dictionary["cooked"],
                            layout_dictionary["cooked"] + layout_dictionary["second_product"],
                            layout_dictionary["cooked"]]
        self.served = 0
        self.Picked = 0
        self.BPicked = 0
        self.cutted = 0
        self.throwed = 0
        self.cook_start = 0
        self.cook_steps = 3
        self.init_layout_as_vector()
        self.init_layout_item_list(self.floors, layout_dictionary["floor"])
        self.init_layout_item_list(self.ovens, layout_dictionary["oven"])
        self.init_layout_item_list(self.cuttings, layout_dictionary["cutting"])

    def init_layout_as_vector(self):
        for i in range(self.rows):
            for j in range(self.columns):
                self.layout_as_vector[i][j][char_dictionary[self.layout[i][j]]] = 1

    def set_agents_first_locations(self, location1, location2):
        self.set_agent_location(location1, 1)
        self.set_agent_location(location2, 2)

    def set_agent_location(self, location, agent_num):
        valid = False
        agent = self.agents[agent_num]

        if 0 <= location.x < self.rows and 0 <= location.y < self.columns and self.layout[location.x][location.y] == \
                layout_dictionary["floor"]:
            self.layout[location.x][location.y] = agent.name

            self.replace_at_layout_item_list(location, layout_dictionary[agent.current_direction])
            self.add_to_layout_item_list(location, agent.current_item)

            agent.location = location
            valid = True
        else:
            self.layout[agent.location.x][agent.location.y] = agent.name
        return valid

    def set_agents_location_randomly(self):
        agents_random_locations = sample(self.floors, 2)

        self.set_agents_first_locations(agents_random_locations[0], agents_random_locations[1])

    def set_agent_location_randomly(self):
        agent_random_locations = sample(self.floors, 1)
        self.set_agent_location(agent_random_locations[0], 1)

    def init_layout_item_list(self, item_list, item_name):
        for i in range(self.rows):
            for j in range(self.columns):
                if self.layout[i][j] == item_name:
                    item_list.append(Location(i, j))

    def replace_at_layout_item_list(self, location, name):
        self.layout_as_vector[location.x, location.y] = 0
        self.add_to_layout_item_list(location, name)

    def add_to_layout_item_list(self, location, name):
        for item in name:
            self.layout_as_vector[location.x][location.y][char_dictionary[item]] = 1

    def move(self, agent_num, where):
        reward = self.scoring["moving"]
        agent = self.agents[agent_num]
        old_location = Location(agent.location.x, agent.location.y)
        new_location = Location(old_location.x, old_location.y)

        old_direction = agent.current_direction
        agent.set_direction(where)

        if where == "UP":
            new_location.x = new_location.x - 1
        elif where == "DOWN":
            new_location.x = new_location.x + 1
        elif where == "LEFT":
            new_location.y = new_location.y - 1
        elif where == "RIGHT":
            new_location.y = new_location.y + 1

        valid = self.set_agent_location(new_location, agent_num)

        if valid:
            self.layout[old_location.x, old_location.y] = layout_dictionary["floor"]
            self.replace_at_layout_item_list(old_location, layout_dictionary["floor"])
        elif old_direction == agent.current_direction:
            reward -= self.scoring["moving"]

        return reward

    def use(self, agent_num, loc):
        score = self.scoring["illegal"]

        if self.rows > loc.x >= 0 and self.columns > loc.y >= 0:
            used = self.layout[loc.x, loc.y]
            origin = used[:1]
            added = used[1:]
            agent = self.agents[agent_num]

            if (origin == layout_dictionary["first_product"] or
                    origin == layout_dictionary["second_product"] or
                    origin == layout_dictionary["plate"]) and agent.current_item == "":
                self.pick(agent, origin)
                score = self.scoring["pick"]
            elif origin == layout_dictionary["table"]:
                if added == "" and agent.current_item != "":
                    score = self.scoring["put_bad"]
                    '''
                    if agent.current_item[0] == layout_dictionary["plate"] and len(agent.current_item) == 2:
                       score = self.scoring["put_good"]
                    '''

                    self.put(agent, loc)
                elif added != "" and agent.current_item == "":
                    self.pick(agent, added)
                    self.layout[loc.x, loc.y] = origin
                    self.replace_at_layout_item_list(loc, origin)

                    score = self.scoring["pick_from_table"]
            elif origin == layout_dictionary["cutting"]:
                if added == "" and (agent.current_item == layout_dictionary["first_product"] or
                                    agent.current_item == layout_dictionary["second_product"]):
                    self.cut(agent, loc)
                    score = self.scoring["cut"]
                elif added != "" and 0 < len(agent.current_item) < 2 and\
                        agent.current_item[0] == layout_dictionary["plate"]:
                    self.pick_to_plate(agent, added)
                    self.layout[loc.x, loc.y] = origin
                    self.replace_at_layout_item_list(loc, origin)

                    score = self.scoring["pick_cut"]
                    score += self.check_meal(agent)
            elif origin == layout_dictionary["oven"]:
                if added == "" and len(agent.current_item) > 0 and \
                        agent.current_item[0] == layout_dictionary["plate"] and \
                        agent.current_item[1:] == layout_dictionary["first_product"]:
                    self.cook(agent, loc)
                    score = self.scoring["cook_start"]
                elif added != "" and 0 < len(agent.current_item) < 3 and \
                        agent.current_item[0] == layout_dictionary["plate"]:
                    self.pick_to_plate(agent, added)
                    self.layout[loc.x, loc.y] = origin
                    self.replace_at_layout_item_list(loc, origin)

                    if added == layout_dictionary["cooked"]:
                        score = self.scoring["cook_finished"]
                    score += self.check_meal(agent)

            elif origin == layout_dictionary["serve"] and 0 < len(agent.current_item) and \
                    agent.current_item[1:] in self.valid_meals:
                self.serve(agent, loc)
                score = self.scoring["serve_good"]
            elif origin == layout_dictionary["garbage"] and agent.current_item != "":
                agent.set_item("")
                score = self.scoring["throw"]
                self.throwed += 1

            self.layout[agent.location.x, agent.location.y] = agent.name
            self.replace_at_layout_item_list(agent.location, layout_dictionary[agent.current_direction])
            self.add_to_layout_item_list(agent.location, agent.current_item)

        return score

    def set(self, loc, item):
        self.layout[loc.x, loc.y] = item
        self.replace_at_layout_item_list(loc, item)

    def put(self, agent, loc):
        self.layout[loc.x, loc.y] += agent.current_item
        self.add_to_layout_item_list(loc, agent.current_item)

        agent.set_item("")

    def pick(self, agent, item):
        agent.set_item(item)
        self.Picked += 1

    def pick_to_plate(self, agent, item):
        agent.set_item(agent.current_item + item)

    def cut(self, agent, loc):
        self.layout[loc.x, loc.y] = layout_dictionary["cutting"] + agent.current_item
        self.add_to_layout_item_list(loc, agent.current_item)
        agent.set_item("")
        self.cutted += 1

    def cook(self, agent, loc):
        self.layout[loc.x, loc.y] += '0'
        self.add_to_layout_item_list(loc, '0')
        self.cook_start += 1
        agent.set_item(layout_dictionary["plate"])

    def serve(self, agent, loc):
        agent.set_item("")
        self.served += 1

    def cook_step(self, loc):

        curr_step = self.layout[loc.x, loc.y][1:]

        if curr_step != "" and curr_step != layout_dictionary["burned"]:
            if curr_step == layout_dictionary["cooked"] and False:
                curr_step = layout_dictionary["burned"]
            elif curr_step.isdigit():
                curr_step = int(curr_step) + 1
                if curr_step == self.cook_steps:
                    curr_step = layout_dictionary["cooked"]

            self.layout[loc.x, loc.y] = layout_dictionary["oven"] + str(curr_step)

            self.replace_at_layout_item_list(loc,  layout_dictionary["oven"])
            self.add_to_layout_item_list(loc, str(curr_step))

    def do_all_cook_steps(self):
        for oven in self.ovens:
            self.cook_step(oven)

    def get_use_location(self, agent_num):
        agent = self.agents[agent_num]
        use_location = Location(agent.location.x, agent.location.y)

        if agent.current_direction == "UP":
            use_location.x = use_location.x - 1
        elif agent.current_direction == "DOWN":
            use_location.x = use_location.x + 1
        elif agent.current_direction == "LEFT":
            use_location.y = use_location.y - 1
        elif agent.current_direction == "RIGHT":
            use_location.y = use_location.y + 1
        return use_location

    def check_meal(self, agent):
        score = 0
        if agent.current_item[1:] in self.valid_meals:
            score = self.scoring["finish_meal"]

        return score


class Location:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class Agent:
    def __init__(self, x, y, current_direction):
        self.location = Location(x, y)
        self.name = layout_dictionary[current_direction]
        self.current_direction = current_direction
        self.current_item = ""

    def set_item(self, item):
        self.current_item = item
        self.set_name()

    def set_direction(self, direction):
        self.current_direction = direction
        self.set_name()

    def set_name(self):
        self.name = layout_dictionary[self.current_direction] + self.current_item
