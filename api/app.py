import os
import os.path
import torch as torch
import torch.nn as nn
from torch.autograd import Variable
import torch.optim as optim
import torch.nn.functional as F

import dill
from flask import Flask, jsonify, request, send_file
from kitchen.dictionary import layout_dictionary
from kitchen.environment import Environment
import tensorflow as tf
from tensorflow.keras.models import load_model
from tensorflow.keras.models import Model, load_model
from tensorflow.keras.layers import Input, Dense, Lambda, Add
from tensorflow.keras.optimizers import Adam, RMSprop
from tensorflow.keras import backend as K

from contextlib import suppress
from os import path
import random
import csv

import numpy as np

from kitchen.actions import TWO_PLAYER_ACTIONS

from kitchen.actions import PLAYER_ACTIONS
from kitchen.environment1 import Environment1

app = Flask(__name__)

Q_TABLE = "Q-Table"
TENSOR = "Tensor"
TORCH = "Torch"


@app.route('/kitchen/layout')
def get_kitchen_layout():
    return {'layout': ""}


@app.route('/kitchen/dictionary')
def get_kitchen_dictionary():
    return {'dictionary': layout_dictionary}


@app.route('/models')
def get_models_list():
    dirs = [name for name in os.listdir(
        "./models") if os.path.isdir('./models/' + name)]
    return jsonify(dirs)


@app.route('/models/<string:model_name>/get')
def get_model(model_name):
    env = Environment("./models/{}/layout.txt".format(model_name), 10)

    file = open(
        "./models/{}/num_of_agents.txt".format(model_name))
    num_of_agents = file.read()
    file.close()

    return {'layout': env.kit.start_layout.tolist(), 'agents': num_of_agents}


@app.route('/models/<string:model_name>/run/<int:x1>/<int:y1>/<int:x2>/<int:y2>')
def run_model_with_two_agents(model_name, x1, y1, x2, y2):
    print(tf.__version__)
    steps = 300
    env = Environment("./models/{}/layout.txt".format(model_name), steps)
    state_size = env.observation_space
    state = env.reset_with_agents_locations(x1, y1, x2, y2)

    results = run_model(model_name, env, TWO_PLAYER_ACTIONS, state, state_size)

    return jsonify({'results': results})


@app.route('/models/<string:model_name>/run/<int:x1>/<int:y1>')
def run_model_with_one_agent(model_name, x1, y1):
    steps = 300
    env = Environment1("./models/{}/layout.txt".format(model_name), steps)
    state_size = env.observation_space
    state = env.reset_with_agent_location(x1, y1)

    results = run_model(model_name, env, PLAYER_ACTIONS, state, state_size)

    return jsonify({'results': results})


@app.route('/agents', methods=['POST'])
def set_agents_first_location():
    print(request.json)


@app.route('/models/<string:model_name>/get_progress')
def get_model_state_progress(model_name):
    progress_episodes = []
    progress_states = []

    with open("./models/{}/progress.csv".format(model_name), newline='', encoding="utf8") as f:
        reader = csv.reader(f)
        data = list(reader)

    ep = data[0][0]
    for row in data:
        if row[0] != ep:
            ep = row[0]
            progress_episodes.append(progress_states)
            progress_states = []
        progress_states.append({'state': row[2], 'reward': row[3]})
    progress_episodes.append(progress_states)

    return jsonify({'progress_states': progress_episodes})


@app.route('/models/<string:model_name>/get_graph')
def get_graph(model_name):
    return send_file("./models/{}/graphs/graph1.PNG".format(model_name), mimetype='image/gif')


def run_model(model_name, env, actions, state, state_size):
    results = []
    done = False
    total = 0
    state = np.reshape(state, [1, state_size])
    model_type = get_model_type(model_name)

    if model_type == TENSOR:
        model_path = "./models/{}/model.h5".format(model_name)
        #model = load_model(model_path)
        model = OurModel(input_shape=(env.kit.state_space,),
                         action_space=len(actions), dueling=True)
        model.load_weights(model_path)
    elif model_type == Q_TABLE:
        model_path = "./models/{}/model.pkl".format(model_name)
        with open(model_path, 'rb') as in_strm:
            q_table = dill.load(in_strm)
    elif model_type == TORCH:
        model_path = "./models/{}/model.pt".format(model_name)
        model = ActorCritic(num_inputs=env.kit.state_space, num_actions=len(actions), hidden_size=24)
        model.load_state_dict(torch.load(model_path ))
        # model.eval()

    data = {'state': env.kit.layout.tolist(
    ), 'served': env.kit.served, 'reward': total}
    results.append(data)
    while not done:
        #env.render()
        if model_type == TENSOR:
            action = np.argmax(model.predict(state))
        elif model_type == Q_TABLE:
            state = get_unique(state, env.kit.state_space)
            action = random.randint(0, len(actions) - 1)
            if state in q_table.keys():
                action = np.argmax(q_table[state])
        elif model_type == TORCH:
            value, policy_dist = model(state)
            dist = policy_dist.detach().numpy()
            action = np.random.choice(len(actions), p=np.squeeze(dist))
            # print(action)
        next_state, reward, done = env.step(actions[action])
        total += reward
        data = {'state': env.kit.layout.tolist(
        ), 'served': env.kit.served, 'reward': total}
        results.append(data)
        state = np.reshape(next_state, [1, state_size])
    return results


def get_model_type(model_name):
    for file in os.listdir("./models/{}".format(model_name)):
        if file.endswith(".h5"):
            return TENSOR
        if file.endswith(".pkl"):
            return Q_TABLE
        if file.endswith(".pt"):
            return TORCH
        


def get_unique(vec, state_space):
    my_string = ""
    vec = np.reshape(vec, state_space)
    for digit in vec:
        my_string += str(int(digit))
    return int(my_string, 2)


def OurModel(input_shape, action_space, dueling):
    X_input = Input(input_shape)
    X = X_input

    # 'Dense' is the basic form of a neural network layer
    # Input Layer of state size(4) and Hidden Layer with 512 nodes
    X = Dense(512, input_shape=input_shape, activation="relu",
              kernel_initializer='he_uniform')(X)

    # Hidden layer with 256 nodes
    X = Dense(256, activation="relu", kernel_initializer='he_uniform')(X)

    # Hidden layer with 64 nodes
    X = Dense(64, activation="relu", kernel_initializer='he_uniform')(X)

    if dueling:
        state_value = Dense(1, kernel_initializer='he_uniform')(X)
        state_value = Lambda(lambda s: K.expand_dims(
            s[:, 0], -1), output_shape=(action_space,))(state_value)

        action_advantage = Dense(
            action_space, kernel_initializer='he_uniform')(X)
        action_advantage = Lambda(lambda a: a[:, :] - K.mean(a[:, :], keepdims=True), output_shape=(action_space,))(
            action_advantage)

        X = Add()([state_value, action_advantage])
    else:
        # Output Layer with # of actions: 2 nodes (left, right)
        X = Dense(action_space, activation="linear",
                  kernel_initializer='he_uniform')(X)

    model = Model(inputs=X_input, outputs=X)  # , name='Dueling DDQN model')
    model.compile(loss="mean_squared_error", optimizer=RMSprop(lr=0.00025, rho=0.95, epsilon=0.01),
                  metrics=["accuracy"])

    model.summary()
    return model

class ActorCritic(nn.Module):
    def __init__(self, num_inputs, num_actions, hidden_size, learning_rate=3e-4):
        super(ActorCritic, self).__init__()
        self.num_actions = num_actions

        self.actor_linear1 = nn.Linear(num_inputs, hidden_size)
        self.actor_linear2 = nn.Linear(hidden_size, hidden_size * 2)
        self.actor_linear3 = nn.Linear(hidden_size * 2, hidden_size)
        self.actor_linear4 = nn.Linear(hidden_size, num_actions)


        self.critic_linear1 = nn.Linear(num_inputs, hidden_size)
        self.critic_linear2 = nn.Linear(hidden_size, hidden_size * 2)
        self.critic_linear3 = nn.Linear(hidden_size * 2, hidden_size)
        self.critic_linear4 = nn.Linear(hidden_size, 1)

    
    def forward(self, state):
        state = Variable(torch.from_numpy(state).float().unsqueeze(0))
        value = F.relu(self.critic_linear1(state))
        value = F.relu(self.critic_linear2(value))
        value = F.relu(self.critic_linear3(value))
        value = self.critic_linear4(value)
        
        policy_dist = F.relu(self.actor_linear1(state))
        policy_dist = F.relu(self.actor_linear2(policy_dist))
        policy_dist = F.relu(self.actor_linear3(policy_dist))
        policy_dist = F.softmax(self.actor_linear4(policy_dist), dim=2)

        return value, policy_dist

