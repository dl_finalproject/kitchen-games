import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import {
    useParams
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
}));

export default function ModelGraphsPage() {
    const classes = useStyles();
    const { modelId } = useParams();

    return (
        <Grid container direction='row' justify='center' alignItems='center'>
            <Grid item>
                <img src={`/models/${modelId}/get_graph`} alt='graph' width={700}/>
            </Grid>
        </Grid>
    );
}
