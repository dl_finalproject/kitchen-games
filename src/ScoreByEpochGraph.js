import React from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';
import { Grid } from '@material-ui/core';
import { makeStyles, Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    title: {
        fontWeight: '600',
    },
}));

const ScoreByEpochGraph = ({ scoreByEpochData }) => {
    const classes = useStyles();

    return (
        <Grid container direction='column' alignItems='center' alignContent='center'>
            <Grid item>
                <Typography className={classes.title} variant='h6' align='center'>Score by Epoch</Typography>
            </Grid>
            <Grid item>
                <LineChart width={600} height={300} data={scoreByEpochData}>
                    <Line type="monotone" stroke="#8884d8" dataKey='FirstAgentScore' />
                    <Line type="monotone" stroke="#82ca9d" dataKey='SecondAgentScore' />
                    <CartesianGrid stroke="#ccc" />
                    <XAxis dataKey="Epoch" />
                    <YAxis />
                    <Legend />
                    <Tooltip
                        labelFormatter={(label) => `Epoch: ${label}`}
                    />
                </LineChart>
            </Grid>
        </Grid>
    );
};

export default ScoreByEpochGraph;