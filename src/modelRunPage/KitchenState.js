import React from "react";
import useStyles from "./modelRunPageStyles";
import { Grid, Avatar, Tooltip } from "@material-ui/core";

const getCellColorClass = (dictionary, classes) => (cell) => {
  // eslint-disable-next-line default-case
  switch (cell) {
    case dictionary.table:
      return { class: classes.table, title: "שולחן" };
    case dictionary.plate:
      return { class: classes.plate, title: "עמדת צלחות" };
    case dictionary.floor:
      return { class: classes.floor, title: "ריצפה" };
    case dictionary.cutting:
      return { class: classes.cuttingTable, title: "עמדת חיתוך" };
    case dictionary.garbage:
      return { class: classes.garbage, title: "פח אשפה" };
    case dictionary.oven:
      return { class: classes.oven, title: "תנור" };
    case dictionary.first_product:
      return { class: classes.firstIngredient, title: "עמדת רכיב ראשון" };
    case dictionary.second_product:
      return { class: classes.secondIngredient, title: "עמדת רכיב שני" };
    case dictionary.serve:
      return { class: classes.serve, title: "עמדת הגשה" };
    default:
      return { class: "direction", title: "סוכן" };
  }
};

export default function KitchenState({ state, dictionary, onCellClick }) {
  const classes = useStyles();

  return (
    <>
      {state.map((row, rowIndex) => {
        return (
          <Grid container>
            {row.map((cell, colIndex) => {
              const cellData = getCellColorClass(dictionary, classes)(cell);
              return (
                <Tooltip title={cellData.title}>
                  <Grid
                    item
                    className={`${classes.cell} ${cellData.class}`}
                    onClick={() => {
                      if (cell === dictionary.floor) {
                        onCellClick(rowIndex, colIndex);
                      }
                    }}
                  >
                    {cellData.class === "direction" ? cell : ""}
                  </Grid>
                </Tooltip>
              );
            })}
          </Grid>
        );
      })}
    </>
  );
}
