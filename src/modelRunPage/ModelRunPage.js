import React, { useState, useEffect, useRef } from "react";
import useStyles from "./modelRunPageStyles";
import { Grid, Button, Switch, FormControlLabel } from "@material-ui/core";
import { useParams } from "react-router-dom";
import axios from "axios";
import KitchenState from "./KitchenState";

const getKitchenLayout = async (modelId) =>
  axios.get(`/models/${modelId}/get`).then((response) => response.data);

const getKitchenDictionary = async () =>
  axios.get("/kitchen/dictionary").then((response) => response.data);

const getProgress = async (modelId) =>
  axios
    .get(`/models/${modelId}/get_progress`)
    .then((response) => response.data);

const calculateTwoAgentRun = async (modelId, firstPos, secondPos) =>
  axios
    .get(
      `/models/${modelId}/run/${firstPos.row}/${firstPos.col}/${secondPos.row}/${secondPos.col}`
    )
    .then((res) => {
      return res.data.results;
    });

const calculateOneAgentRun = async (modelId, pos) =>
  axios.get(`/models/${modelId}/run/${pos.row}/${pos.col}`).then((res) => {
    return res.data.results;
  });

const initialProgress = {
  episode: 1,
  state: 1,
};

const playerHasPos = (pos) => pos.row !== -1 && pos.col !== -1;
const initialPos = {
  row: -1,
  col: -1,
};

export default function ModelRunPage() {
  const classes = useStyles();
  const { modelId } = useParams();
  const [kitchen, setKitchen] = useState({
    layout: [],
    agents: 0,
    dictionary: {},
    progress: [],
  });
  const [currentProgress, setCurrentProgress] = useState(initialProgress);
  const [isPlaying, setIsPlaying] = useState(false);
  const progressInterval = useRef(null);
  const [runMode, setRunMode] = useState(false);
  const [currentPosPick, setCurrentPosPick] = useState(1);
  const [firstPlayerPos, setFirstPlayerPos] = useState(initialPos);
  const [secondPlayerPos, setSecondPlayerPos] = useState(initialPos);
  const [canCalculateRun, setCanCalculateRun] = useState(false);
  const [runResult, setRunResult] = useState([]);
  const [runProgress, setRunProgress] = useState(0);

  useEffect(() => {
    if (isPlaying && kitchen.progress.length) {
      clearInterval(progressInterval.current);
      progressInterval.current = setInterval(() => {
        const moveToNextEpisode =
          currentProgress.state ===
          kitchen.progress[currentProgress.episode - 1].length;
        const nextState = moveToNextEpisode ? 1 : currentProgress.state + 1;
        const nextEpisode = moveToNextEpisode
          ? currentProgress.episode + 1
          : currentProgress.episode;

        if (nextEpisode > kitchen.progress.length) {
          clearInterval(progressInterval.current);
          setIsPlaying(false);
        } else {
          setCurrentProgress({
            episode: nextEpisode,
            state: nextState,
          });
        }
      }, 10);
    }
  }, [
    kitchen.progress,
    currentProgress.state,
    currentProgress.episode,
    isPlaying,
  ]);

  useEffect(() => {
    if (isPlaying && runResult.length) {
      clearInterval(progressInterval.current);
      progressInterval.current = setInterval(() => {
        const moveToNextState = runProgress + 1 !== runResult.length;

        if (!moveToNextState) {
          clearInterval(progressInterval.current);
          setIsPlaying(false);
        } else {
          setRunProgress(runProgress + 1);
        }
      }, 100);
    }
  }, [runResult, runProgress, isPlaying]);

  useEffect(() => {
    const fetchData = async () => {
      const [
        { layout, agents },
        { dictionary },
        { progress_states },
      ] = await Promise.all([
        getKitchenLayout(modelId),
        getKitchenDictionary(modelId),
        getProgress(modelId),
      ]);
      setKitchen({
        layout,
        agents: Number(agents),
        dictionary,
        progress: progress_states,
      });
    };

    fetchData();
  }, [modelId]);

  useEffect(() => {
    if (canCalculateRun) {
      if (kitchen.agents === 2) {
        calculateTwoAgentRun(modelId, firstPlayerPos, secondPlayerPos).then(
          (result) => {
            setRunResult(result);
          }
        );
      } else {
        calculateOneAgentRun(modelId, firstPlayerPos).then((result) => {
          setRunResult(result);
        });
      }
    }
  }, [
    canCalculateRun,
    firstPlayerPos,
    secondPlayerPos,
    modelId,
    kitchen.agents,
  ]);

  const playProgress = () => {
    if (!isPlaying) {
      setIsPlaying(true);
    }
  };
  const pauseProgress = () => {
    setIsPlaying(false);
  };
  const restartProgress = () => {
    clearInterval(progressInterval.current);
    if (runMode) {
      setRunProgress(0);
    } else {
      setCurrentProgress(initialProgress);
    }
    setIsPlaying(false);
  };

  let currentState;
  let currentReward;
  let served = 0;
  if (runMode) {
    served = runResult.length ? runResult[runProgress].served : 0;
    currentReward = runResult.length ? runResult[runProgress].reward : 0;
    currentState = runResult.length
      ? runResult[runProgress].state
      : kitchen.layout;
  } else {
    currentReward = kitchen.progress.length
      ? kitchen.progress[currentProgress.episode - 1][currentProgress.state - 1]
          .reward
      : 0;
    currentState = kitchen.progress.length
      ? eval(
          kitchen.progress[currentProgress.episode - 1][
            currentProgress.state - 1
          ].state
        )
      : kitchen.layout;
  }

  return (
    <Grid container direction="column" justify="center" align-items="center">
      <Grid item>
        <Grid container spacing={2}>
          <Grid item>
            <Button onClick={playProgress} variant="contained" color="primary">
              Play
            </Button>
          </Grid>
          <Grid item>
            <Button onClick={pauseProgress} variant="outlined" color="primary">
              Pause
            </Button>
          </Grid>
          <Grid item>
            <Button onClick={restartProgress} color="secondary">
              Restart
            </Button>
          </Grid>
        </Grid>
      </Grid>
      <Grid item>
        <FormControlLabel
          label="Run mode"
          control={
            <Switch
              checked={runMode}
              onChange={() => {
                setRunMode(!runMode);
                setCurrentPosPick(1);
                setFirstPlayerPos(initialPos);
                setSecondPlayerPos(initialPos);
                restartProgress();
                setRunResult([]);
              }}
              color="primary"
            />
          }
        />
        {(playerHasPos(firstPlayerPos) && kitchen.agents === 1) ||
        (playerHasPos(firstPlayerPos) && playerHasPos(secondPlayerPos)) ? (
          <Button onClick={() => setCanCalculateRun(true)}>
            Calculate run
          </Button>
        ) : null}

        {playerHasPos(firstPlayerPos) || playerHasPos(secondPlayerPos) ? (
          <>
            <Button
              onClick={() => {
                setCurrentPosPick(1);
                setFirstPlayerPos(initialPos);
                setSecondPlayerPos(initialPos);
                restartProgress();
                setRunResult([]);
                setCanCalculateRun(true);
              }}
            >
              Clear agent positions
            </Button>
            <div>
              First agent position:{" "}
              {firstPlayerPos.row + ", " + firstPlayerPos.col}
            </div>
            {kitchen.agents === 2 ? (
              <div>
                Second agent position:{" "}
                {secondPlayerPos.row + ", " + secondPlayerPos.col}
              </div>
            ) : null}
          </>
        ) : null}
      </Grid>
      <Grid item className={classes.kitchen}>
        {!runMode ? (
          <>
            <div>
              current episode:{" "}
              {kitchen.progress.length ? currentProgress.episode : ""}
            </div>
            <div>
              current state:{" "}
              {kitchen.progress.length ? currentProgress.state : ""}
            </div>
            <div>current reward: {currentReward}</div>
          </>
        ) : (
          <>
            <div>current state: {runResult.length ? runProgress + 1 : ""}</div>
            <div>current reward: {currentReward}</div>
            <div>dishes served: {served}</div>
          </>
        )}
        <KitchenState
          onCellClick={(rowIndex, colIndex) => {
            if (runMode) {
              if (currentPosPick === 1) {
                setFirstPlayerPos({ row: rowIndex, col: colIndex });
                if (kitchen.agents === 2) {
                  setCurrentPosPick(2);
                }
              } else if (currentPosPick === 2) {
                setSecondPlayerPos({ row: rowIndex, col: colIndex });
                setCurrentPosPick(1);
              }
            }
          }}
          state={currentState}
          dictionary={kitchen.dictionary}
        />
      </Grid>
    </Grid>
  );
}
