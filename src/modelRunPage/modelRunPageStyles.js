import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    kitchen: {
        marginTop: theme.spacing(4),
    },
    cell: {
        width: 64,
        height: 64,
    },
    table: {
        backgroundColor: 'burlywood'
    },
    plate: {
        backgroundColor: 'lightskyblue'
    },
    floor: {
        backgroundColor: 'lightgray'
    },
    cuttingTable: {
        backgroundColor: 'red'
    },
    garbage: {
        backgroundColor: 'green'
    },
    oven: {
        backgroundColor: 'yellow'
    },
    firstIngredient: {
        backgroundColor: 'purple'
    },
    secondIngredient: {
        backgroundColor: 'pink'
    },
    serve: {
        backgroundColor: 'lightgreen'
    }
}));