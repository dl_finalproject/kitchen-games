import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import {
  CssBaseline,
  AppBar,
  Toolbar,
  List,
  Typography,
  Divider,
} from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ModelListItem from "./ModelListItem";
import ModelRunPage from "./modelRunPage/ModelRunPage";
import ModelGraphsPage from "./ModelGraphsPage";
import axios from "axios";

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
  },
  appBar: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerPaper: {
    width: drawerWidth,
  },
  // necessary for content to be below app bar
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
  unStyledLink: {
    textDecoration: "none",
  },
}));

export default function App() {
  const classes = useStyles();
  const [models, setModels] = useState([]);

  useEffect(() => {
    axios.get("/models").then((response) => {
      setModels(response.data);
    });
  }, []);

  return (
    <div className={classes.root}>
      <Router>
        <CssBaseline />
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <Typography variant="h6" noWrap>
              Kitchen Games
            </Typography>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="permanent"
          classes={{
            paper: classes.drawerPaper,
          }}
          anchor="left"
        >
          <div className={classes.toolbar} />
          <Divider />
          <List>
            {models.map((model) => (
              <ModelListItem modelId={model} name={model} />
            ))}
          </List>
        </Drawer>
        <main className={classes.content}>
          <div className={classes.toolbar} />
          <Switch>
            <Route exact path="/"></Route>
            <Route path="/model/:modelId/run">
              <ModelRunPage />
            </Route>
            <Route path="/model/:modelId/graphs">
              <ModelGraphsPage />
            </Route>
          </Switch>
        </main>
      </Router>
    </div>
  );
}
