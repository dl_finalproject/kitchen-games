import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, ListItemIcon, ListItemText, Collapse, ListItemAvatar, Avatar } from '@material-ui/core';
import { ExpandLess, ExpandMore, TrendingDown, DirectionsRun } from '@material-ui/icons';
import {
    Link
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    nested: {
        paddingLeft: theme.spacing(4),
    },
}));

export default function ModelListItem({ modelId, name, onRunModelClick, onModelGraphsClick }) {
    const classes = useStyles();
    const [open, setOpen] = useState(false);

    const handlChange = () => {
        setOpen(!open);
    }

    return (
        <>
            <ListItem button onClick={handlChange}>
                <ListItemAvatar>
                    <Avatar>1</Avatar>
                </ListItemAvatar>
                <ListItemText primary={name} />
                {open ? <ExpandLess /> : <ExpandMore />}
            </ListItem>
            <Collapse in={open} timeout="auto" unmountOnExit>
                <List component="div" disablePadding>
                    <ListItem
                        button
                        className={classes.nested}
                        component={Link}
                        to={`/model/${modelId}/run`}
                    >
                        <ListItemIcon>
                            <DirectionsRun />
                        </ListItemIcon>
                        <ListItemText primary="Run The Model" />
                    </ListItem>
                    <ListItem
                        button
                        className={classes.nested}
                        component={Link}
                        to={`/model/${modelId}/graphs`}
                    >
                        <ListItemIcon>
                            <TrendingDown />
                        </ListItemIcon>
                        <ListItemText primary="Graphs" />
                    </ListItem>
                </List>
            </Collapse>
        </>
    );
}
